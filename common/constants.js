const { AvenirMQ_ALL } = require("./common");

exports.errCode = {
    UNKNOWN_ERROR: {
        code: -100,
        message: 'unknown error'
    },
    BAD_REQUEST: {
        code: -101,
        message: 'bad request'
    },
    AVENIR_SUCCESS: {
        code: 0,
        message: 'success',
    },
    BAD_COMMAND: {
        code:-1,
        message:'bad command',
    },

    //用户管理类错误
    USER_EXISTS: {
        code: 100,
        message: 'user is already exists',
    },
    USER_NOW_FOUND: {
        code: 101,
        message: 'user not found',
    },
    INVALID_LOGIN: {
        code: 102,
        message: 'invalid name or password'
    },

    //程序错误
    BAD_KEYS: {
        code: 1,
        message: 'bad AvenirMQ keys,key must be [from.to.key]',
    },
    TIMEOUT_ERROR: {
        code: 2,
        message: 'AvenirMQ timeout',
    },
    INVALID_SIGN: {
        code: 3,
        message: 'invalid sign or being cleared',
    },
    KEY_ANY_ERROR: {
        code: 4,
        message: 'AvenirMQ bind key error:[to] cant be #',
    }

}

exports.succCode = 0;
exports.succMessage = 'success';
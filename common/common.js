const { questionNewPassword } = require('readline-sync');
const constants = require('./constants');
exports.getError = (type, strict) => {
    this.toLog("需要获取的error为 ", type);
    if (!constants.errCode[type]) {
        if (type === undefined) {
            type = 'UNKNOWN_ERROR';
        } else {
            throw {
                code: -44944,
                message: type
            }
        }
    }
    if (strict) {
        throw ({
            code: constants.errCode[type].code,
            message: constants.errCode[type].message
        })
    }

    return {
        code: constants.errCode[type].code,
        message: constants.errCode[type].message
    }
}

//是否输出日志
exports.toLog = (...args) => {
    !ini.mq.ifConsoleLog || console.log(args);
}

exports.SUCCESS = 'AVENIR_SUCCESS';

exports.delQuotation = (value) => {
    value = value.toString().replace(/\"/g, "");
    value = value.toString().replace(/\'/g, "");
    return value;
}

//获取签名
exports.getSign = (name, password) => {
    let sign = name + password + Math.random().toString().slice(3);
    return sign;
}

//key存在该字符则代表接收所有这样的消息
exports.AvenirMQ_ALL = '#'

exports.safeJsonStringfy = (json) => {
    try {
        return JSON.stringify(json);
    } catch (error) {
        return json;
    }
}

exports.safeJsonParse = (json) => {
    try {
        return JSON.Parse(json);
    } catch (error) {
        return json;
    }
}
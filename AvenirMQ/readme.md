# avenirmq

开源项目AvenirMQ的操作库 [AvenirMQ](https://gitee.com/onlyyyy/AvenirMQ)

## APIS

1. async init(info)

为了告诉avenirmq目前的连接信息

```js
async init(info) {
    this.ip = info.ip;
    this.port = info.port;
}
```


2. async login(info)

登录并获取签名，info传name和password 

返回sign

3. async send(data, sign)

将数据发送给AvenirMQ

```js
async send(data, sign) {
    let sendText = {
        type: 'send',
        sign: sign || this.sign,
        data: data
    }
    let res = await this.asyncSend(sendText, 'send');
    return res;
}
```

4. async receive(info, func)

调用此函数会启动一个服务，当数据从AvenirMQ回来的时候会调用func，即func为目前的回调函数

```js
async receive(info, func) {
    let { ip, port } = info;
    if(!ip || !port) {
        throw ('Object[info] must contain ip and port');
    }

    if(!func) {
        throw ('lack of callback func');
    }
    this.server = new net.createServer();
    this.server.on('connection', async (client) => {

        client.on('data', async (msg) => { 
            //接收client发来的信息
            func(JSON.parse(msg));
        });

        client.on('error', function (e) { 
            //监听客户端异常
            console.log('client error:' + e);
            client.end();
        });

        client.on('close', function () {

        });

    });
    this.server.listen(port, ip, function () {

    });
}
```



## example

```js
async function main() {
    avenirmq.init({ ip: '127.0.0.1', port: '52013' });
    let res = await avenirmq.login({
        name: "test",
        password: '11111111'
    });
    console.log("res = ", res);
    let sign = res.sign;
    await avenirmq.receive({
        ip: '127.0.0.1',
        port: '13000',
    }, cout);
    await avenirmq.send({a:'123'},sign);
   

}

function cout(data) {
    console.log('data :>> ', data);
}

main();

```